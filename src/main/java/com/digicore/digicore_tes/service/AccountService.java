package com.digicore.digicore_tes.service;

import com.digicore.digicore_tes.model.entity.Accounts;
import com.digicore.digicore_tes.model.request.*;
import com.digicore.digicore_tes.model.response.AccoountInfo;
import com.digicore.digicore_tes.model.response.AccountInfoResponse;
import com.sun.org.apache.regexp.internal.RE;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;


@Service
public class AccountService {

    private Map<String, Accounts> account;
    private Map<String, String > accountInfo;
    private AccountInfoResponse accountInfoResponse;
    private ResponseEntity responseEntity;
    Accounts createAccount = new Accounts();
    private String id;
    long accountNumber;
    Double balance = 0.0;

    public ResponseEntity createAccount(CreateAccountRequest accountRequest) throws Exception {
                id = UUID.randomUUID().toString();
        if (account != null){
        accountInfoResponse = new AccountInfoResponse();
        accountInfoResponse.setResponseCode(409);
        accountInfoResponse.setSuccess(false);
        accountInfoResponse.setMessage("Duplicate ID");
         responseEntity = ResponseEntity.status(HttpStatus.CONFLICT).body(accountInfoResponse);
            System.out.println();
        return responseEntity;

        }
             accountNumber = generateAccountNumber();
            account = new HashMap<>();
            createAccount.setAccountName(accountRequest.getAccountName());
            createAccount.setAccountPassword(accountRequest.getAccountPassword());
            createAccount.setMessage("Accounts Number " + accountNumber + " successfully created.");
            createAccount.setSuccess(true);
            createAccount.setResponseCode(200);
            createAccount.setId(id);
            createAccount.setAccountNumber(accountNumber);
            createAccount.setBalance(balance);
        account.put(id,createAccount);
         accountInfoResponse = new AccountInfoResponse();
        accountInfoResponse.setResponseCode(createAccount.getResponseCode());
            accountInfoResponse.setSuccess(createAccount.isSuccess());
            accountInfoResponse.setMessage(createAccount.getMessage());
         responseEntity = ResponseEntity.status(HttpStatus.OK).body(accountInfoResponse);
        System.out.println(responseEntity.toString());
        System.out.println(id);
        return responseEntity;
    }

    public ResponseEntity getAccountInformation(AccountInfoRequest accountInfoRequest)throws Exception{
//        AccountInfoRequest request = new AccountInfoRequest();
//        createAccount = new Accounts();
        System.out.println("account number " +createAccount.getAccountNumber());
        System.out.println("request "+ accountInfoRequest.getAccountNumber());
        if (accountInfoRequest.getAccountNumber().equals(createAccount.getAccountNumber().toString())) {
            AccoountInfo accoountInfo = new AccoountInfo();
            accoountInfo.setAccountNumber(createAccount.getAccountName());
            accoountInfo.setAccountName(createAccount.getAccountName());
            accoountInfo.setBalance(createAccount.getBalance());
             accountInfoResponse = new AccountInfoResponse();
            accountInfoResponse.setAccount(accoountInfo);
            accountInfoResponse.setResponseCode(200);
            accountInfoResponse.setMessage("Account information Fetched successfully");
            accountInfoResponse.setSuccess(true);
            responseEntity = ResponseEntity.status(HttpStatus.OK).body(accountInfoResponse);
        }
        if (account.containsValue(accountInfoRequest.getAccountNumber()) && account.containsValue(accountInfoRequest.getAccountPassword())){
            accountInfoResponse.setMessage("account Not Found");
            accountInfoResponse.setResponseCode(400);
            responseEntity = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(accountInfoResponse);
        }
        responseEntity = ResponseEntity.status(HttpStatus.OK).body(accountInfoResponse);
       return responseEntity;
    }

    public AccountInfoResponse getAccountStatement(AccountInfoRequest AccountInfoRequest)throws Exception{
        account = new HashMap<>();
        AccountInfoResponse accountInfo = new AccountInfoResponse();
        for (Map.Entry pairKeyForAccountInfo: account.entrySet()){
            accountInfo = (AccountInfoResponse) pairKeyForAccountInfo.getValue();

        }
        if (accountInfo.equals("") || accountInfo.equals("null")){
            accountInfo.setResponseCode(400);
            accountInfo.setMessage(" Accounts Not Found");
        }
        return accountInfo;
    }


    public ResponseEntity depositToAccount(AccountDepositRequest  accountDepositRequest) throws Exception{
        System.out.println(accountDepositRequest.getAmount());
        if (accountDepositRequest.getAmount() >  1000000.00){
            System.out.println("Great");
        }
        if ((accountDepositRequest.getAmount() > 1000000.00) || (accountDepositRequest.getAmount() < 1.00)){
            accountInfoResponse.setResponseCode(400);
            accountInfoResponse.setSuccess(false);
            accountInfoResponse.setMessage("This amount can not be processed, Please input a valid amount and try again!");
            responseEntity = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(accountInfoResponse);
            return responseEntity;
        }
        account = new HashMap<>();
        Accounts createAccount = new Accounts();
        createAccount.setMessage("Your deposit of " + accountDepositRequest.getAmount() + " was successful");
        createAccount.setSuccess(true);
        createAccount.setResponseCode(200);
        createAccount.setId(id);
        createAccount.setBalance(accountDepositRequest.getAmount());
        createAccount.setAccountNumber(accountNumber);
        account.put(id,createAccount);
        System.out.println("new Balance " + createAccount.getBalance());
       balance = balance + createAccount.getBalance();
        accountInfoResponse = new AccountInfoResponse();
        accountInfoResponse.setResponseCode(createAccount.getResponseCode());
        accountInfoResponse.setSuccess(createAccount.isSuccess());
        accountInfoResponse.setMessage(createAccount.getMessage());
        accountInfoResponse.setBalance(balance.toString());
        responseEntity = ResponseEntity.status(HttpStatus.OK).body(accountInfoResponse);
        System.out.println(responseEntity.toString());
        System.out.println(id);
        return responseEntity;
    }

    public ResponseEntity withdrwalFromAccount(AccountWithdrawalRequest accountWithdrawalRequest){
        System.out.println("balance "+ balance);
        if ((accountWithdrawalRequest.getWithdrawnAmount()) < balance && accountWithdrawalRequest.getWithdrawnAmount() < 500){
            accountInfoResponse.setMessage("account Not Found");
            accountInfoResponse.setResponseCode(400);
            responseEntity = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(accountInfoResponse);
        }
        accountInfoResponse = new AccountInfoResponse();
        accountInfoResponse.setResponseCode(200);
        accountInfoResponse.setMessage("Account information Fetched successfully");
        accountInfoResponse.setSuccess(true);
        responseEntity = ResponseEntity.status(HttpStatus.OK).body(accountInfoResponse);
        return responseEntity;

    }



    private long generateAccountNumber() throws Exception {
        Integer accountNumber = 0;
        accountNumber = (int)((Math.random() * 9000000)+1000000000);
        return accountNumber;

    }

    }
