package com.digicore.digicore_tes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DigicoreTesApplication {

	public static void main(String[] args) {
		SpringApplication.run(DigicoreTesApplication.class, args);
	}

}
