package com.digicore.digicore_tes.model.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 *
 * @author Muyiwa
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountOperationResponse {

    private int responseCode;
    private boolean success;
    private String message;

    public static AccountOperationResponse fromAccount(Map<String, String> accounts){
        AccountInfoResponse accountInfoResponse = new AccountInfoResponse();
        accounts.put(String.valueOf(accountInfoResponse.getResponseCode()), accounts.get("responseCode"));
        accounts.put("success", String.valueOf(true));
        accounts.put(accountInfoResponse.getMessage(),accounts.get("message"));
//        accounts.put(String.valueOf(accountInfoResponse.getAccount()),accounts.get("account"));
        return (AccountOperationResponse) accounts;
    }
}
