package com.digicore.digicore_tes;

import java.util.Scanner;

public class PerfectSquareCount {


    static int countSquares(int l, int b)
    {
        // If n is smaller, swap m and n
        if (b < l)
        {
            // swap(m, n)
            int temp = l;
            l= b;
            b = temp;
        }


        // Now n is greater dimension,
        // apply formula
        return l * (l + 1) * (2 * l + 1) /
                6 + (b - l) * l * (l + 1) / 2;
    }

    // Driver Code
    public static void main(String[] args)
    {

        Scanner input = new Scanner(System.in);

        System.out.printf("Please enter the length of your grid : ");
        int l = input.nextInt();
        System.out.printf("Please enter the breath of your grid : ");
        int b = input.nextInt();
//        System.out.println("count " +);
        System.out.println("Count of squares is " +
                countSquares(l, b));
    }

}
